<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

## CRUD Laravel y VUEjs

<p>En este curso encontrarás 16 videos prácticos donde programamos las funciones editar, crear, eliminar, listar elementos y por último paginar. Laravel 5.4 es la versión usada en este curso para el Backend y Vue.js versión 2 (framework javascript muy popular y apoyado por Laravel) lo usaremos para desarrollar el Frontend y darle dinamismo a nuestro proyecto.</p>

<p>Nota: Yo utilizo la version de Larvel 5.8</p>
